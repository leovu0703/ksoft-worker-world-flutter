import UIKit
import Flutter
import GoogleMaps

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
    GMSServices.provideAPIKey(gmsMapKey)
    let viewFactory = GoogleMapNativeViewFactory()
    registrar(forPlugin: "GoogleMapNV").register(viewFactory, withId: "GoogleMapNativeView")
    NativeMap.shared.setupAnalyzing(window: window)
    MarkerTap.shared.setupAnalyzing(window: window)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
