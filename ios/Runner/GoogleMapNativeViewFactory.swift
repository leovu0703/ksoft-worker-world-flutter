//
//  GoogleMapNativeViewFactory.swift
//  Runner
//
//  Created by Long Vu on 3/27/19.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import UIKit
import Flutter
import GoogleMaps

extension AppDelegate {
    public class GoogleMapNativeView: NSObject, FlutterPlatformView , GMSMapViewDelegate {
        let frame:CGRect
        let viewId:Int64
        
        var mapView:GMSMapView?
        
        init(_ frame: CGRect, viewId:Int64, args: Any?) {
            self.frame = frame
            self.viewId = viewId
        }
        public func view() -> UIView {
            
            mapView = GMSMapView.init(frame: frame)
            
            mapView?.isMyLocationEnabled = true
            mapView?.settings.myLocationButton = true
            mapView?.delegate = self
            
            mapView?.tag = 1010
            
            if let locValue:CLLocationCoordinate2D = CLLocationManager().location?.coordinate {
                let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude), zoom: 16.0, bearing: 0, viewingAngle: 0)
                mapView?.camera = camera
            }
            
            do {
                // Set the map style by passing the URL of the local file.
                if let styleURL = Bundle.main.url(forResource: "ggMapStyle", withExtension: "json") {
                    mapView?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                } else {
                    NSLog("Unable to find style.json")
                }
            } catch {
                NSLog("One or more of the map styles failed to load. \(error)")
            }
            
            return mapView ?? UIView()
        }
        
        public func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
            mapView.selectedMarker = marker
            return true
        }
        
        public func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
            let infoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?.first! as! CustomInfoWindow
            return infoWindow
        }
        public func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
            MarkerTap.shared.marker = marker
            NotificationCenter.default.post(name: Notification.Name.MarkerGoogleMapTappedInfoWindow, object: nil)
        }
    }
    public class GoogleMapNativeViewFactory: NSObject,FlutterPlatformViewFactory {
        public func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
            return GoogleMapNativeView(frame, viewId: viewId, args: args)
        }
    }
}

class NativeMap {
    static let shared = NativeMap()
    func setupAnalyzing(window:UIWindow) {
        guard let controller = window.rootViewController as? FlutterViewController else {
            fatalError("rootViewController is not type FlutterViewController")
        }
        let channel = FlutterMethodChannel(name: "flutter.io/loadMarker",
                                                  binaryMessenger: controller)
        channel.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            guard call.method == "loadMarker" else {
                result(FlutterMethodNotImplemented)
                return
            }
            self?.loadMarkers(call: call, vc: controller, result: result)
        })
    }
    private func loadMarkers(call:FlutterMethodCall ,vc:FlutterViewController,result: @escaping FlutterResult) {
        
        guard let topVC = UIApplication.topViewController() else { result("Error"); return }
        guard let mapview:GMSMapView =  topVC.view.viewWithTag(1010) as? GMSMapView else { result("Error"); return }
        guard let data = call.arguments as? [String:Any] else { result("Error"); return }
        
        
        //TODO: Kiem tra array markers ton tai
        guard let markers = data["markers"] as? [[String:Any]] else { result("Error"); return }
        
        mapview.clear()
        
        for value in markers {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: value["lat"] as? Double ?? 0, longitude:  value["long"] as? Double ?? 0)
            marker.map = mapview
            
            marker.title = "Hãy xác nhận sẵn sàng"
            marker.infoWindowAnchor = CGPoint(x: -1.25 , y: 0.5)
            marker.userData = value
        }
        
        //TODO: Kiem tra polyline ton tai
        guard let points = data["polyline"] as? String else { result("Error"); return }
        
        let path = GMSPath(fromEncodedPath: points)
        let polyline = GMSPolyline(path:path)
        polyline.strokeColor = .purple
        polyline.strokeWidth = 5.0
        polyline.map = mapview
        
        var bounds = GMSCoordinateBounds()
        for index in 1...path!.count() {
            bounds = bounds.includingCoordinate(path!.coordinate(at: index))
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
        mapview.moveCamera(update)
        
        result("Success")
    }
}

class MarkerTap: FlutterAppDelegate,FlutterStreamHandler {
    
    static let shared = MarkerTap()
    var marker:GMSMarker?
    private var eventSink: FlutterEventSink?
    
    func setupAnalyzing(window:UIWindow) {
        guard let controller = window.rootViewController as? FlutterViewController else {
            fatalError("rootViewController is not type FlutterViewController")
        }
        let channel = FlutterEventChannel(name: "flutter.io/markerTap",
                                                  binaryMessenger: controller)
        channel.setStreamHandler(self)
    }
    
    public func onListen(withArguments arguments: Any?,
                         eventSink: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = eventSink
        UIDevice.current.isBatteryMonitoringEnabled = true
        sendBatteryStateEvent()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.onMarkerTapChanged),
            name: NSNotification.Name.MarkerGoogleMapTappedInfoWindow,
            object: nil)
        return nil
    }
    
    
    @objc private func onMarkerTapChanged(notification: NSNotification) {
        sendBatteryStateEvent()
    }
    
    private func sendBatteryStateEvent() {
        guard let eventSink = eventSink else {
            return
        }
        guard let curMark = marker else { return }
        marker = nil
        guard let dic = curMark.userData as? [String:Any] else { return }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            let jsonString = String(data: jsonData, encoding: .utf8)
            eventSink(jsonString)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    public func onCancel(withArguments arguments: Any?) -> FlutterError? {
        NotificationCenter.default.removeObserver(self)
        eventSink = nil
        return nil
    }
}


extension Notification.Name {
    static let MarkerGoogleMapTappedInfoWindow = Notification.Name("marker-tap-infowindow")
}
