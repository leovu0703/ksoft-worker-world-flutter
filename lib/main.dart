import 'package:flutter/material.dart';
import 'modules/tabbar/src/ui/tabbar.dart';
import 'modules/splash/src/ui/splash_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'K-soft Worker',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: TabbarPage(0),
      home: SplashScreen(),
    );
  }

}
