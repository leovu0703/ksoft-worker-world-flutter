import 'package:flutter/material.dart';
import '../common/constant.dart';

class MaterialSwitch extends StatefulWidget {
  const MaterialSwitch(
      {Key key,
      @required this.options,
      this.height,
      this.onSelect,
      this.selectedBackgroundColor,
      this.unselectedBackgroundColor,
      this.selectedTextColor,
      this.unselectedTextColor,
      this.style})
      : super(key: key);

  final List<String> options;
  final ValueChanged<int> onSelect;
  final Color selectedBackgroundColor;
  final Color selectedTextColor;
  final Color unselectedBackgroundColor;
  final Color unselectedTextColor;
  final TextStyle style;
  final double height;

  @override
  _MaterialSwitchState createState() => new _MaterialSwitchState(options);
}

class _MaterialSwitchState extends State<MaterialSwitch> {
  final List<String> options;
  _MaterialSwitchState(this.options);
  int _pos = 0;

  void _onTap(int pos) {
    setState(() {
      _pos = pos;
    });
    widget.onSelect(_pos);
  }

  List<Widget> _setupSwitch() {
    return options
        .asMap()
        .map((i, element) => MapEntry(
            i,
            ButtonTheme(
              minWidth: 50.0,
              height: widget.height,
              child: FlatButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  ),
                  textColor: _pos == i
                      ? widget.selectedTextColor
                      : widget.unselectedTextColor,
                  color: _pos == i
                      ? widget.selectedBackgroundColor
                      : widget.unselectedBackgroundColor,
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      widget.options[i],
                      style: widget.style,
                    ),
                  ),
                  onPressed: () {
                    _onTap(i);
                  }),
            )))
        .values
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: widget.height,
        decoration: BoxDecoration(
          border: Border.all(color: orangeAppColor, width: 1.0),
          borderRadius: BorderRadius.all(
            Radius.circular(widget.height / 2),
          ),
        ),
        child: Padding(
            padding: EdgeInsets.only(left: 1.0, top: 1.0, bottom: 1.0,right: 1.0),
            child: Container(
              child: Row(children: _setupSwitch()),
            )));
  }
}
