import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import '../../../../../../ultility/hexColor.dart';
import '../../../../../../common/constant.dart';

class WalletPage extends StatefulWidget {
  final double contentHeight;
  WalletPage({this.contentHeight});
  @override
  WalletPageState createState() =>
      WalletPageState(contentHeight: contentHeight);
}

class WalletPageState extends State<WalletPage> {
  final double contentHeight;
  WalletPageState({this.contentHeight});

  @override
  Widget build(BuildContext context) {
    return _setupLayout(context);
  }

  // TODO: Setup Layout

  Widget _setupAvatarButton() {
    return FloatingActionButton(
        elevation: 0.0,
        backgroundColor: Colors.white,
        child: Container(
          decoration: BoxDecoration(shape: BoxShape.circle),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10000000.0),
            child: CachedNetworkImage(
              fit: BoxFit.fill,
              imageUrl: 'https://data.whicdn.com/images/299530921/large.png',
              placeholder: (context, url) => Transform(
                  alignment: FractionalOffset.center,
                  transform: Matrix4.identity()..scale(0.35, 0.35),
                  child: CupertinoActivityIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
        onPressed: () {});
  }

  Widget _buildTopView() {
    return Container(
      height: 50.0,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(width: 75.0),
          Container(
            height: 15.0,
            width: 30.0,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/ico_VND.png'),
                    fit: BoxFit.cover)),
          ),
          Container(width: 8.0),
          Text('4.000.000đ', style: TextStyle(fontWeight: FontWeight.bold)),
          Container(width: 8.0),
          Container(
            height: 20.0,
            width: 20.0,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/icon-plus.png'),
                    fit: BoxFit.fill)),
          ),
          Container(width: 10.0),
          _setupAvatarButton(),
        ],
      ),
    );
  }

  Widget _setupHistoryButton() {
    return Padding(
      padding: EdgeInsets.only(bottom: 8.0),
      child: MaterialButton(
        child: Container(
          decoration: BoxDecoration(
              color: HexColor("1F487D"),
              borderRadius: BorderRadius.all(Radius.circular(12.5))),
          height: 25.0,
          width: 140.0,
          child: Center(
            child: Text(
              'Lịch sử công việc',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        onPressed: () {},
      ),
    );
  }

  List<Widget> _setupBottomView() {
    return [
      Container(height: 20.0),
      Container(
          color: HexColor("F26100"),
          height: 1.0,
          width: MediaQuery.of(context).size.width / 5),
      Padding(
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: Text(
          '45%',
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w400, fontSize: 16.0),
        ),
      ),
      Container(
          color: HexColor("F26100"),
          height: 1.0,
          width: MediaQuery.of(context).size.width / 5),
      Container(height: 20.0),
      Padding(
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: Container(
          width: MediaQuery.of(context).size.width * 8.5 / 10,
          child: Row(
            children: <Widget>[
              Text(
                'Số tiền thưởng',
                style: TextStyle(color: Colors.red),
              ),
              Expanded(child: Container()),
              Text(
                '540.000đ',
                style: TextStyle(color: Colors.red),
              ),
            ],
          ),
        ),
      ),
      Padding(
          padding: EdgeInsets.only(top: 5.0, bottom: 20.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 8.5 / 10,
            child: Text(
              'Mỗi lần hoàn thành công việc cây sẽ tăng lên 1%, sau khi cây đạt 50% thì bạn có thể rung tiền thưởng.\nSau khi cây đạt 50% và cứ 5 lần hoàn thành công việc bạn sẽ được tiếp tục rung',
              textAlign: TextAlign.center,
            ),
          )),
      MaterialButton(
        child: Container(
            height: 40.0,
            width: 160.0,
            decoration: BoxDecoration(
                color: orangeAppColor,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Center(
              child: Text(
                'RUNG CÂY TIỀN',
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            )),
        onPressed: () {},
      ),
      Container(height: 20.0)
    ];
  }

  Widget _setupGifView() {
    return Expanded(
      child: Image.asset('images/tree/tree_0_9.gif'),
    );
  }

  Widget _setupLayout(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          title: Text(
            'Số tiền trong tài khoản',
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
        body: Container(
          height: contentHeight,
          child: Column(
            children: List.unmodifiable(() sync* {
              yield _buildTopView();
              yield _setupHistoryButton();
              yield _setupGifView();
              yield* _setupBottomView();
            }()),
          ),
        ));
  }
}
