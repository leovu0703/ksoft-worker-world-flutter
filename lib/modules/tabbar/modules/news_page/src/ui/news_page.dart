import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import '../../../../../../ultility/hexColor.dart';
import '../../../../../../common/constant.dart';

class NewsPage extends StatefulWidget {
  final double contentHeight;
  NewsPage({this.contentHeight});
  @override
  NewsPageState createState() => NewsPageState(contentHeight: contentHeight);
}

class NewsPageState extends State<NewsPage> {
  final double contentHeight;
  NewsPageState({this.contentHeight});

  // TODO: Setup Layout

  Widget _setupBackground() {
    return Container(
      height: contentHeight * 2 / 3,
      width: MediaQuery.of(context).size.width,
      child: Image.asset('images/img_bg.png', fit: BoxFit.fill),
    );
  }

  Widget _setupNewsView() {
    return Container(
      width: MediaQuery.of(context).size.width * 8.5 / 10,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    width: 10.0,
                    child: Center(
                      child: Container(
                        height: 10.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: orangeAppColor),
                      ),
                    )),
                Expanded(
                    child: Container(
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Text('Chấp nhận thợ làm việc',
                        textAlign: TextAlign.left, maxLines: 1),
                  ),
                )),
                Container(
                  child: Container(
                    child: Icon(Icons.arrow_forward_ios,
                        color: Colors.black26, size: 15.0),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Divider(color: orangeAppColor),
          )
        ],
      ),
    );
  }

  Widget _setupNewsImageView() {
    return Container(
      width: MediaQuery.of(context).size.width * 8.5 / 10,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    width: 10.0,
                    child: Center(
                      child: Container(
                        height: 10.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: orangeAppColor),
                      ),
                    )),
                Expanded(
                    child: Container(
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Text('Chấp nhận thợ làm việc',
                        textAlign: TextAlign.left, maxLines: 1),
                  ),
                )),
                Container(
                  child: Container(
                    child: Icon(Icons.arrow_forward_ios,
                        color: Colors.black26, size: 15.0),
                  ),
                )
              ],
            ),
          ),
          Container(
              height: contentHeight * 1 / 6,
              child: FittedBox(
                fit: BoxFit.fill,
                child: CachedNetworkImage(
                  imageUrl:
                      'https://placeit-assets.s3-accelerate.amazonaws.com/landing-pages/make-a-twitch-banner2/Twitch-Banner-Blue-1024x324.png',
                  placeholder: (context, url) => Transform(
                      alignment: FractionalOffset.center,
                      transform: Matrix4.identity()..scale(0.35, 0.35),
                      child: CupertinoActivityIndicator()),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              )),
          Padding(
            padding: EdgeInsets.only(left: 10.0, right: 10.0),
            child: Divider(color: orangeAppColor),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: HexColor("#E6E6E6"),
          title: Text(
            'Thông báo',
            style: TextStyle(fontSize: 16.0, color: Colors.black),
          ),
        ),
        body: Container(
          height: contentHeight,
          color: Colors.white,
          child: Stack(
            children: <Widget>[
              _setupBackground(),
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 20.0,
                      color: Colors.transparent,
                    ),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0))),
                        width: MediaQuery.of(context).size.width * 8.5 / 10,
                        child: ListView(
                          children: <Widget>[
                            _setupNewsView(),
                            _setupNewsImageView(),
                            _setupNewsView(),
                            _setupNewsView(),
                            _setupNewsImageView(),
                            _setupNewsView(),
                            _setupNewsView(),
                            _setupNewsImageView(),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
