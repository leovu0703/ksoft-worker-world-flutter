import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import '../../../../../../ultility/hexColor.dart';
import 'dart:io' show Platform;

class HomePage extends StatefulWidget {
  final double contentHeight;
  HomePage({this.contentHeight});
  @override
  NativeMapState createState() => NativeMapState(contentHeight: contentHeight);
}

class NativeMapState extends State<HomePage> {
  final double contentHeight;
  NativeMapState({this.contentHeight});

  double _topWidgetHeight = 60.0;

  static const MethodChannel loadMarkerChannel =
      MethodChannel('flutter.io/loadMarker');
  static const EventChannel eventChannel = EventChannel('flutter.io/markerTap');

  @override
  void initState() {
    super.initState();
    eventChannel.receiveBroadcastStream().listen(_onEvent, onError: _onError);
  }

  void _onEvent(Object event) {
    Map<String, dynamic> data = jsonDecode(event);
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => SubApp(
              markerData: data,
            )));
  }

  void _onError(Object error) {
    print(error);
  }

  @override
  Widget build(BuildContext context) {
    return _setupLayout(context);
  }

  // TODO: Load Marker
  Future<void> _loadMarkerMap() async {
    var params = <String, dynamic>{
      "markers": [
        {
          "lat": 51.050657,
          "long": 10.649514,
        },
        {
          "lat": 50.618895,
          "long": 9.214272,
        }
      ],
      "polyline":
          "{_rvHwz~_AoFm^cErCyJxGaVpPeWjQgNxUsMdP{^~Gcg@aEqiAgUs@OiJiA?yD|BeAOjz@{A|T_AnJvA|@dEzBrA~FaEtNqCbC?xAbU`_@jIdOhZvSlT|a@tKhb@jEb\\jNpOdVpEzp@hh@hNtMhI|\\r^xlAnVlaBnP`v@lOrGpFdf@fA`ErNzGfHdBtCdDdOzCtPkIhw@_GzD`BvBzNxB~I_BlACxv@bAp_@`DhS`FhJtF`JwE`jAOt\\xIzm@nHhp@~Xh~Ab]vy@fQtn@lGrk@jFjx@vIhs@hLf_@aJlJoDdIvBlKej@p`Bkh@jmAuJhi@uDdk@\\tt@`JfcA|Bjt@y@rg@e@lpApG|xAzQftAdXloArFrpAoEtu@_PxbB{KzeCfFrw@jQdp@hHle@kWntDwh@rgDaErnB~DdZpUlo@|W|g@vOp^pNbY~\\vOl[`M`TtQzIhPhNjg@lJzPvNrI`a@bC|QzDx[`a@x`@bo@pOxn@pTvhA|@r\\eCpXiN~e@eCvWtAd\\zYt}Ab@zk@fEbb@pQtu@hTno@fSxt@`Cte@RlkAbJ~`@rx@~bAr^j|@vUhJzLdHdJ`QjUbx@vI`\\hOrWxVtw@vXdXrSjc@rWxy@dj@bwAnPhp@dIpyA|D~gAfPf_An\\zcAnXp`@jK`IlYvAnMdH~K`\\xDrXdHtO`OjUvExP~BjbA~FdZpA`[cG`dArP`{@bBxjBBnf@nJveAxGjl@jJ~e@lIvr@jA|hA{Czm@gHrZcDj\\hJ~{@~Qff@|Md\\rCzVrEd|@nAzf@cG`u@xOfiArPpQxP|Adh@hZpSlJtIzJpOji@}@vi@nFf`@z\\bo@~W~e@lFxt@bApf@cCp\\AbZjEhXvRpg@zUxYvLfb@hEpj@dPtp@~JfRzH~P|BnUiK~s@iEnV@fV|DxSnNrWhHbWjLbaAj`@xmCrGv`@rQ|a@rW`t@pX~y@z[|a@pIbTtFr`@ze@zcD}GxjAcExw@`Ins@tKzWdk@rj@tLjX~Djo@~@~j@nH~g@fQ~gA~IrfAEzVyAvCbJbI~G~GHZnEpC~VtMtGj@lKxIpL~[pK|PlVxR~NzPhC`BtJHr_@CrHsDlK_Ad]uPz\\y@dYRnOaI|LaKzNiDvVqCnGgAbIzE`JdFtIdA`Ml`@pK~`@xEpBrDpM`H`UjKsHpC{ArFhCdJnJbC`H|IhRzLnKzIk@lDzGnUyD`M_@dLgHhKkTpD~AjF{H`KaC`JyFbJ_AdBnLvEtCxFi@xWvA|\\}AbH_F|CV~Ej@vAmDd@{HuCuD`ByD"
    };
    String message;
    try {
      message = await loadMarkerChannel.invokeMethod('loadMarker', params);
      print(message);
    } on PlatformException catch (e) {
      message = "Failed to get data from native : '${e.message}'.";
    }
  }

  // TODO: Setup Layout

  Widget _setupTopView() {
    return Container(
      height: _topWidgetHeight,
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            child: Text('Vũ Ngọc Long',
                style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('4.0'),
                Container(width: 3.0),
                ImageIcon(
                  AssetImage("images/icon-star.png"),
                  color: HexColor("#FFD700"),
                  size: 15.0,
                ),
                Container(width: 3.0),
                Text('3.0'),
                Container(width: 3.0),
                ImageIcon(AssetImage("images/icon-heart.png"),
                    color: HexColor("#FF0000"), size: 15.0),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _setupBottomView(BuildContext context) {
    return Container(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: contentHeight - _topWidgetHeight,
        child: Container(
          child: Platform.isIOS
              ? UiKitView(
                  viewType: 'GoogleMapNativeView',
                )
              : AndroidView(
                  viewType: 'GoogleMapNativeView',
                ),
        ),
      ),
    );
  }

  Widget _setupAvatarButton() {
    return FloatingActionButton(
        elevation: 0.0,
        backgroundColor: Colors.white,
        child: Container(
          decoration: BoxDecoration(shape: BoxShape.circle),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10000000.0),
            child: CachedNetworkImage(
              fit: BoxFit.fill,
              imageUrl: 'https://data.whicdn.com/images/299530921/large.png',
              placeholder: (context, url) => Transform(
                  alignment: FractionalOffset.center,
                  transform: Matrix4.identity()..scale(0.35, 0.35),
                  child: CupertinoActivityIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
        onPressed: () {});
  }

  Widget _setupLayout(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: HexColor("#0AA45A"),
        title: Text(
          'Sẵn sàng tìm việc',
          style: TextStyle(fontSize: 16.0),
        ),
      ),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: contentHeight,
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[_setupTopView(), _setupBottomView(context)],
              ),
              Positioned(
                top: AppBar().preferredSize.height - _topWidgetHeight / 2,
                right: 20.0,
                child: _setupAvatarButton(),
              )
            ],
          )),
    );
  }
}

class SubApp extends StatelessWidget {
  // This widget is the root of your application.
  final Map<String, dynamic> markerData;

  SubApp({Key key, @required this.markerData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sub View'),
      ),
      body: Center(
        child: Text("Dữ liệu Marker:" + markerData.toString()),
      ),
    );
  }
}
