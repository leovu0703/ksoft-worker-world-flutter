import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../modules/home_page/src/ui/home_page.dart';
import '../../modules/wallet_page/src/ui/wallet_page.dart';
import '../../modules/news_page/src/ui/news_page.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import '../../../../common/constant.dart';

class TabbarPage extends StatefulWidget {
  final currentSelected;

  TabbarPage(this.currentSelected);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return TabbarPageState();
  }
}

class TabbarPageState extends State<TabbarPage> {
  GlobalKey _keyHeightTabbar = GlobalKey();
  double _sizeTabarHeight = 0;

  // Mockup Data
  List<int> _arrTabId = [1, 2, 3];
  List<String> _arrTabbarImages = [
    "images/icon-home.png",
    "images/icon-wallet.png",
    "images/icon-noti.png",
  ];

  _getSizes() {
    final RenderBox renderBoxRed =
        _keyHeightTabbar.currentContext.findRenderObject();
    setState(() {
      _sizeTabarHeight = renderBoxRed.size.height;
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
    FlutterStatusbarManager.setHidden(true);
    super.initState();
  }

  _afterLayout(_) {
    _getSizes();
  }

  @override
  Widget build(BuildContext context) {
    return _setupLayout();
  }

  // TODO: Setup Layout

  List<BottomNavigationBarItem> _setupCupertinoTabbarBottom() {
    return _arrTabbarImages
        .map((image) =>
            BottomNavigationBarItem(icon: ImageIcon(AssetImage(image))))
        .toList();
  }

  Widget _setupCupertinoTabbarContent(double contentHeight, int index) {
    if (_arrTabId[index] == 1) {
      return Container(child: HomePage(contentHeight: contentHeight));
    } else if (_arrTabId[index] == 2) {
      return Container(child: WalletPage(contentHeight: contentHeight));
    } else if (_arrTabId[index] == 3) {
      return Container(child: NewsPage(contentHeight: contentHeight));
    
    } else if (_arrTabId[index] == 4) {
      return Container(child: NewsPage(contentHeight: contentHeight));
    }  else {
      return Container();
    }
  }

  Widget _setupLayout() {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        key: _keyHeightTabbar,
        activeColor: orangeAppColor,
        items: _setupCupertinoTabbarBottom(),
      ),
      tabBuilder: (BuildContext context, int index) {
        if (_sizeTabarHeight != 0) {
          return CupertinoTabView(
            builder: (BuildContext context) {
              return _setupCupertinoTabbarContent(
                  MediaQuery.of(context).size.height -
                      _sizeTabarHeight -
                      MediaQuery.of(context).padding.top -
                      AppBar().preferredSize.height,
                  index);
            },
          );
        } else {
          return Container();
        }
      },
    );
  }
}
