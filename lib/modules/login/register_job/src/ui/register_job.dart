import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import '../../../../../ultility/hexColor.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../../../../tabbar/src/ui/tabbar.dart';
import '../../../../../common/constant.dart';

class RegisterJob extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterJobState();
  }
}

class RegisterJobState extends State<RegisterJob> {
  FocusNode _focusJobNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  Widget _setupTopBack() {
    return Padding(
      padding: EdgeInsets.only(top: 20.0),
      child: Container(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Container(
                    height: 40.0,
                    width: MediaQuery.of(context).size.width / 8,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20.0),
                            bottomRight: Radius.circular(20.0)),
                        image: DecorationImage(
                            image: AssetImage('images/icon-back.png'),
                            fit: BoxFit.cover)),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 60.0,
                child: Center(
                  child: Text(
                    'Lựa chọn công việc phù hợp mà bạn\ncó thể làm.\nCó thể chọn nhiều công việc.',
                    style: TextStyle(fontSize: 15.0),
                    maxLines: 3,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _searchAddreesView() {
    return Padding(
      padding: EdgeInsets.only(top: 20.0),
      child: Container(
          child: Wrap(
        children: <Widget>[
          Container(
            height: 40.0,
            child: Container(
              height: 40.0,
              width: MediaQuery.of(context).size.width * 8.5 / 10,
              decoration: BoxDecoration(
                  border: Border.all(color: orangeAppColor, width: 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  color: Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      width: 40.0,
                      decoration: BoxDecoration(
                        border: Border.all(color: orangeAppColor, width: 1.0),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20.0),
                            topLeft: Radius.circular(20.0)),
                        color: orangeAppColor,
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Image.asset(
                          'images/ico_map_white.png',
                          fit: BoxFit.contain,
                        ),
                      )),
                  Expanded(
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Container(
                            child: Row(
                          children: <Widget>[
                            Expanded(
                              child: TextField(
                                style: TextStyle(fontWeight: FontWeight.w700),
                                focusNode: _focusJobNode,
                                decoration:
                                    InputDecoration.collapsed(hintText: ''),
                              ),
                            ),
                            Icon(Icons.search, color: orangeAppColor)
                          ],
                        ))),
                  )
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }

  Widget _setupListViewAddressContent() {
    return Padding(
      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
      child: Container(
        height: 100.0,
        child: Column(
          children: <Widget>[
            Container(
              height: 25.0,
              decoration: BoxDecoration(
                color: orangeAppColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(14),
                    topRight: Radius.circular(14)),
              ),
            ),
            Expanded(
                child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15.0),
                      bottomRight: Radius.circular(15.0))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      height: 75.0,
                      width: 75.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(37.5)
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: CachedNetworkImage(
                          fit: BoxFit.contain,
                          imageUrl:
                              'http://www.theworldofwork.co.uk/Portals/_default/Skins/wow/icons/worker-2.png',
                          placeholder: (context, url) => Transform(
                              alignment: FractionalOffset.center,
                              transform: Matrix4.identity()..scale(0.5, 0.5),
                              child: CupertinoActivityIndicator()),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      )),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 5.0),
                      child: Container(
                          child: AutoSizeText(
                        'Thợ chở hàng',
                        maxLines: 1,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16.0),
                        overflow: TextOverflow.ellipsis,
                      )),
                    ),
                  )
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }

  Widget _setupLoginView() {
    return FormKeyboardActions(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      actions: [
        KeyboardAction(
          focusNode: _focusJobNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
      child: SafeArea(
        child: Container(
            color: Colors.transparent,
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      _setupTopBack(),
                      _searchAddreesView(),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 25.0),
                          child: Container(
                              width:
                                  MediaQuery.of(context).size.width * 8.5 / 10,
                              child: Stack(children: [
                                Padding(
                                  padding: EdgeInsets.only(bottom: 20.0),
                                  child: ListView.builder(
                                      itemCount: 6,
                                      physics: ClampingScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return GestureDetector(
                                            //You need to make my child interactive
                                            onTap: () {
                                              print(index);
                                            },
                                            child:
                                                _setupListViewAddressContent());
                                      }),
                                ),
                                Container(
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20.0)),
                                          color: orangeAppColor,
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width *
                                                1 /
                                                2.5,
                                        height: 40.0,
                                        child: Center(
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                left: 5.0, right: 5.0),
                                            child: InkWell(
                                              onTap: () {
                                                Navigator.of(context)
                                                    .pushReplacement(
                                                        MaterialPageRoute(
                                                            maintainState:
                                                                false,
                                                            settings:
                                                                RouteSettings(
                                                              isInitialRoute:
                                                                  true,
                                                            ),
                                                            builder:
                                                                (context) =>
                                                                    TabbarPage(
                                                                        0)));
                                              },
                                              child: AutoSizeText('Hoàn tất',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 20.0,
                                                      fontWeight:
                                                          FontWeight.w500)),
                                            ),
                                          ),
                                        )),
                                  ),
                                )
                              ])),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 20.0, top: 15.0),
                          child: Container(
                            child: Text(
                              'Việc lựa chọn đúng với những vị trí mà bạn có thể đảm\nnhận sẽ giúp hệ thống dễ dàng tìm kiếm vị trí phù hợp\nvới bạn.',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: HexColor('E8E8E8'),
      child: Stack(
        children: <Widget>[_setupLoginView()],
      ),
    ));
  }
}
