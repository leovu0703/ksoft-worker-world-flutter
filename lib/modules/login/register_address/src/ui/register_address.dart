import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import '../../../register_confirm/src/ui/register_confirm.dart';
import '../../../../../common/constant.dart';

class RegisterAddress extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterAddressState();
  }
}

class RegisterAddressState extends State<RegisterAddress> {
  FocusNode _focusAddressNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  Widget _setupTopBack() {
    return Padding(
      padding: EdgeInsets.only(top: 10.0),
      child: Container(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  height: 40.0,
                  width: MediaQuery.of(context).size.width / 8,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20.0),
                          bottomRight: Radius.circular(20.0)),
                      image: DecorationImage(
                          image: AssetImage('images/icon-back.png'),
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 40.0,
                child: Center(
                  child: Text(
                    'Nhập địa chỉ của bạn',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _searchAddreesView() {
    return Padding(
      padding: EdgeInsets.only(top: 10.0),
      child: Container(
          child: Wrap(
        children: <Widget>[
          Container(
            height: 40.0,
            child: Container(
              height: 40.0,
              width: MediaQuery.of(context).size.width * 8.5 / 10,
              decoration: BoxDecoration(
                  border: Border.all(color: orangeAppColor, width: 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  color: Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      width: 40.0,
                      decoration: BoxDecoration(
                        border: Border.all(color: orangeAppColor, width: 1.0),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20.0),
                            topLeft: Radius.circular(20.0)),
                        color: orangeAppColor,
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Image.asset(
                          'images/ico_map_white.png',
                          fit: BoxFit.contain,
                        ),
                      )),
                  Expanded(
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Container(
                          child: TextField(
                            style: TextStyle(fontWeight: FontWeight.w700),
                            focusNode: _focusAddressNode,
                            decoration: InputDecoration.collapsed(
                                hintText: 'Địa chỉ của bạn'),
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }

  Widget _setupListViewAddressContent() {
    return Padding(
      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
      child: Container(
        height: 50,
        child: Column(
          children: <Widget>[
            Expanded(
                child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      child: Padding(
                    padding: EdgeInsets.all(12.5),
                    child: Image.asset('images/ico_address_black.png'),
                  )),
                  Expanded(
                    child: Container(
                        child: Text(
                      '46 Nguyễn Cơ Thạch,Hà Nội,Việt Nam',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    )),
                  )
                ],
              ),
            )),
            Divider(
              color: orangeAppColor,
              height: 1.0,
            ),
          ],
        ),
      ),
    );
  }

  Widget _setupLoginView() {
    return FormKeyboardActions(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      actions: [
        KeyboardAction(
          focusNode: _focusAddressNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
      child: SafeArea(
        child: Container(
            color: Colors.transparent,
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      _setupTopBack(),
                      _searchAddreesView(),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 25.0),
                          child: Container(
                              width:
                                  MediaQuery.of(context).size.width * 8.5 / 10,
                              child: ListView.builder(
                                  itemCount: 6,
                                  physics: ClampingScrollPhysics(),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return GestureDetector(
                                        //You need to make my child interactive
                                        onTap: () {
                                          print(index);
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      RegisterConfirm()));
                                        },
                                        child: _setupListViewAddressContent());
                                  })),
                        ),
                      )
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[_setupLoginView()],
      ),
    ));
  }
}
