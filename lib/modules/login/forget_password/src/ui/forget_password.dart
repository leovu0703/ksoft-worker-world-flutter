import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import '../../../forget_pass_confirm/src/ui/forget_pass_confirm.dart';
import '../../../../../common/constant.dart';

class ForgetPassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ForgetPasswordState();
  }
}

class ForgetPasswordState extends State<ForgetPassword> {
  FocusNode _focusPhoneNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      width: MediaQuery.of(context).size.width,
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/img_bg_login.png'),
                fit: BoxFit.fill)),
      ),
    );
  }

  Widget _setupInputView() {
    return Container(
      child: Column(
        children: _setupInputViewContent(),
      ),
    );
  }

  List<Widget> _setupInputViewContent() {
    return [
      Container(
        child: Stack(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width * 9 / 10,
                child: Container(
                  child: Column(
                    children: List.unmodifiable(() sync* {
                      yield new Container(
                        child: _topInputViewContent(),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[400],
                                blurRadius:
                                    10.0, // has the effect of softening the shadow
                                spreadRadius:
                                    5.0, // has the effect of extending the shadow
                                offset: Offset(
                                  5.0, // horizontal, move right 10
                                  5.0, // vertical, move down 10
                                ),
                              )
                            ]),
                      );
                      yield new Container(height: 20.0);
                    }()),
                  ),
                )),
            Positioned(
              bottom: 0.0,
              left: MediaQuery.of(context).size.width * 9 / 10 / 2 -
                  (MediaQuery.of(context).size.width * 9 / 10 / 2.5) / 2,
              child: Container(
                  height: 40.0,
                  width: (MediaQuery.of(context).size.width * 9 / 10 / 2.5),
                  decoration: BoxDecoration(
                    color: orangeAppColor,
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.only(left: 5.0, right: 5.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ForgetPassConfirm()));
                        },
                        child: AutoSizeText('Tiếp tục',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  )),
            ),
          ],
        ),
      )
    ];
  }

  Widget _topInputViewContent() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 15.0, bottom: 15.0, left: 10.0),
          child: Container(),
        ),
        _inputView('images/icon-phone.png', 'Số điện thoại của bạn'),
        _setupLineView(),
        Padding(
          padding: EdgeInsets.only(bottom: 30.0),
          child: Container(),
        )
      ],
    );
  }

  Widget _setupLineView() {
    return Padding(
      padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 15.0),
      child: Divider(
        height: 1.0,
        color: orangeAppColor,
      ),
    );
  }

  Widget _inputView(String image, String placeholder) {
    return Padding(
      padding: EdgeInsets.only(bottom: 15.0, left: 10.0, right: 10.0),
      child: Container(
        height: (MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top -
                MediaQuery.of(context).padding.bottom) /
            30,
        child: Row(
          children: <Widget>[
            Container(
              height: 30.0,
              width: 30.0,
              child: Image.asset(
                image,
                fit: BoxFit.contain,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: 12.0, right: 12.0, top: 2.0, bottom: 2.0),
              child: Container(color: orangeAppColor, width: 1.0),
            ),
            Padding(
              padding: EdgeInsets.only(right: 12.0),
              child: Text('(+84)'),
            ),
            Expanded(
              child: TextField(
                  focusNode: _focusPhoneNode,
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration.collapsed(hintText: placeholder),
                  inputFormatters: image == 'images/icon-phone.png'
                      ? [
                          LengthLimitingTextInputFormatter(11),
                        ]
                      : []),
            )
          ],
        ),
      ),
    );
  }

  Widget _setupTopBack() {
    return Padding(
      padding: EdgeInsets.only(top: 10.0),
      child: Container(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  height: 40.0,
                  width: MediaQuery.of(context).size.width / 8,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20.0),
                          bottomRight: Radius.circular(20.0)),
                      image: DecorationImage(
                          image: AssetImage('images/icon-back.png'),
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 40.0,
                child: Center(
                  child: Text(
                    'Quên mật khẩu',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _setupLoginView() {
    return FormKeyboardActions(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      actions: [
        KeyboardAction(
          focusNode: _focusPhoneNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
      child: SafeArea(
        child: Container(
            color: Colors.transparent,
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      _setupTopBack(),
                      Container(
                        height: (MediaQuery.of(context).size.height -
                                MediaQuery.of(context).padding.top -
                                MediaQuery.of(context).padding.bottom) /
                            9,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0, bottom: 30.0),
                        child: Text('Vui lòng nhập số điện thoại của bạn'),
                      ),
                      _setupInputView(),
                      Expanded(
                          child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(),
                      )),
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[_setupBackground(), _setupLoginView()],
      ),
    ));
  }
}
