import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import '../../../../../ultility/materialSwitch.dart';
import '../../../forget_password/src/ui/forget_password.dart';
import '../../../register_address/src/ui/register_address.dart';
import '../../../../../common/constant.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterPageState();
  }
}

class RegisterPageState extends State<RegisterPage> {
  FocusNode _focusNameNode = FocusNode();
  FocusNode _focusAddressNode = FocusNode();
  FocusNode _focusPhoneNode = FocusNode();
  FocusNode _focusPasswordNode = FocusNode();
  FocusNode _focusRePasswordNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  Widget _setupBackground() {
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/registerScreen.png'),
                fit: BoxFit.cover)));
  }

  Widget _setupRegisterForgetPasswordView() {
    return Container(
      child: Padding(
        padding:
            EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0, top: 20.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            MaterialButton(
              child: Text('Đăng nhập',
                  style: TextStyle(color: Colors.black, fontSize: 17.0)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            MaterialButton(
              child: Text('Quên mật khẩu',
                  style: TextStyle(color: orangeAppColor, fontSize: 17.0)),
              onPressed: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => ForgetPassword()));
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _setupInputView() {
    return Container(
      child: Column(
        children: _setupInputViewContent(),
      ),
    );
  }

  List<Widget> _setupInputViewContent() {
    return [
      Container(
        child: Stack(
          children: <Widget>[
            Container(
                child: Container(
              width: MediaQuery.of(context).size.width * 9 / 10,
              child: Column(
                children: List.unmodifiable(() sync* {
                  yield new Container(
                    child: _topInputViewContent(),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[400],
                            blurRadius:
                                10.0, // has the effect of softening the shadow
                            spreadRadius:
                                5.0, // has the effect of extending the shadow
                            offset: Offset(
                              5.0, // horizontal, move right 10
                              5.0, // vertical, move down 10
                            ),
                          )
                        ]),
                  );
                  yield new Container(height: 20.0);
                }()),
              ),
            )),
            Positioned(
              bottom: 0.0,
              left: MediaQuery.of(context).size.width * 9 / 10 / 2 -
                  (MediaQuery.of(context).size.width * 9 / 10 / 2.5) / 2,
              child: Container(
                  height: 40.0,
                  width: (MediaQuery.of(context).size.width * 9 / 10 / 2.5),
                  decoration: BoxDecoration(
                    color: orangeAppColor,
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.only(left: 5.0, right: 5.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => RegisterAddress()));
                        },
                        child: AutoSizeText('Đăng ký',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  )),
            ),
          ],
        ),
      )
    ];
  }

  Widget _topInputViewContent() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 15.0, bottom: 15.0, left: 10.0),
          child: Align(
              child: Text('Đăng ký',
                  style:
                      TextStyle(fontSize: 18.0, fontWeight: FontWeight.w700)),
              alignment: Alignment.centerLeft),
        ),
        _inputView('images/icon-name.png', 'Họ và tên', false),
        _setupLineView(),
        _inputView('images/icon-location.png', 'Địa chỉ', false),
        _setupLineView(),
        _inputView('images/icon-phone.png', 'Số điện thoại của bạn', false),
        _setupLineView(),
        _inputView('images/icon-password.png', 'Mật khẩu', false),
        _setupLineView(),
        _inputView('images/icon-password.png', 'Nhập lại mật khẩu', true),
        _setupLineView(),
        Padding(
          padding:
              EdgeInsets.only(bottom: 15.0, top: 5.0, left: 10.0, right: 10.0),
          child: Container(
            height: 40.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Giới tính',
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: orangeAppColor),
                ),
                Expanded(
                  child: Container(),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: MaterialSwitch(
                    height: 40.0,
                    options: <String>['Nữ', 'Nam', 'Không rõ'],
                    unselectedBackgroundColor: Colors.white,
                    selectedBackgroundColor: orangeAppColor,
                    unselectedTextColor: orangeAppColor,
                    selectedTextColor: Colors.white,
                    onSelect: (int selectIndex) {
                      setState(() {});
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 30.0),
          child: Container(
            height: 40.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Đăng nhập bằng tài khoản',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Container(
                    height: 25.0,
                    width: 25.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('images/icon-fb.png'),
                            fit: BoxFit.cover)),
                    child: MaterialButton(
                      onPressed: () {},
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _setupLineView() {
    return Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 15.0),
      child: Divider(
        height: 1.0,
        color: orangeAppColor,
      ),
    );
  }

  Widget _inputView(String image, String placeholder, bool repass) {
    return Padding(
      padding: EdgeInsets.only(bottom: 15.0, left: 10.0, right: 10.0),
      child: Container(
        height: (MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top -
                MediaQuery.of(context).padding.bottom) /
            30,
        child: Row(
          children: <Widget>[
            Container(
              height: 30.0,
              width: 30.0,
              child: Image.asset(
                image,
                fit: BoxFit.contain,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: 12.0, right: 12.0, top: 2.0, bottom: 2.0),
              child: Container(color: orangeAppColor, width: 1.0),
            ),
            Expanded(
              child: TextField(
                  focusNode: image == 'images/icon-name.png'
                      ? _focusNameNode
                      : image == 'images/icon-location.png'
                          ? _focusAddressNode
                          : image == 'images/icon-phone.png'
                              ? _focusPhoneNode
                              : repass
                                  ? _focusRePasswordNode
                                  : _focusPasswordNode,
                  keyboardType: image == 'images/icon-phone.png'
                      ? TextInputType.phone
                      : TextInputType.text,
                  obscureText:
                      image == 'images/icon-password.png' ? true : false,
                  decoration: InputDecoration.collapsed(hintText: placeholder),
                  inputFormatters: image == 'images/icon-phone.png'
                      ? [
                          LengthLimitingTextInputFormatter(11),
                        ]
                      : []),
            )
          ],
        ),
      ),
    );
  }

  Widget _setupLoginView() {
    return FormKeyboardActions(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      actions: [
        KeyboardAction(
          focusNode: _focusNameNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusAddressNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusPhoneNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusPasswordNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusRePasswordNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
      child: SafeArea(
        child: Container(
            color: Colors.transparent,
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                          child: Align(
                        alignment: Alignment.topCenter,
                        child: Container(),
                      )),
                      _setupInputView(),
                      _setupRegisterForgetPasswordView()
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[_setupBackground(), _setupLoginView()],
      ),
    ));
  }
}
