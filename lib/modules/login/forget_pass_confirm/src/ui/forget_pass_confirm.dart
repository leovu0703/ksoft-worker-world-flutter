import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import '../../../../../common/constant.dart';

class ForgetPassConfirm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ForgetPassConfirmState();
  }
}

class ForgetPassConfirmState extends State<ForgetPassConfirm> {
  FocusNode _focusPhoneNode = FocusNode();
  FocusNode _focusPasswordNode = FocusNode();
  FocusNode _focusRepasswordNode = FocusNode();
  FocusNode _focusOTP1Node = FocusNode();
  FocusNode _focusOTP2Node = FocusNode();
  FocusNode _focusOTP3Node = FocusNode();
  FocusNode _focusOTP4Node = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      width: MediaQuery.of(context).size.width,
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/img_bg_login.png'),
                fit: BoxFit.fill)),
      ),
    );
  }

  Widget _setupInputView() {
    return Container(
      child: Column(
        children: _setupInputViewContent(),
      ),
    );
  }

  List<Widget> _setupInputViewContent() {
    return [
      Container(
        child: Stack(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width * 9 / 10,
                child: Container(
                  child: Column(
                    children: List.unmodifiable(() sync* {
                      yield new Container(
                        child: _topInputViewContent(),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey[400],
                                blurRadius:
                                    10.0, // has the effect of softening the shadow
                                spreadRadius:
                                    5.0, // has the effect of extending the shadow
                                offset: Offset(
                                  5.0, // horizontal, move right 10
                                  5.0, // vertical, move down 10
                                ),
                              )
                            ]),
                      );
                      yield new Container(height: 20.0);
                    }()),
                  ),
                )),
            Positioned(
              bottom: 0.0,
              left: MediaQuery.of(context).size.width * 9 / 10 / 2 -
                  (MediaQuery.of(context).size.width * 9 / 10 / 2.5) / 2,
              child: Container(
                  height: 40.0,
                  width: (MediaQuery.of(context).size.width * 9 / 10 / 2.5),
                  decoration: BoxDecoration(
                    color: orangeAppColor,
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.only(left: 5.0, right: 5.0),
                      child: AutoSizeText('Tiếp tục',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold)),
                    ),
                  )),
            ),
          ],
        ),
      )
    ];
  }

  Widget _topInputViewContent() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 15.0, bottom: 15.0, left: 10.0),
          child: Container(),
        ),
        _inputView(
            'images/icon-vietnam-flag.png', 'Số điện thoại của bạn', false),
        _setupLineView(),
        Padding(
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          child: _inputViewOTPText(),
        ),
        Padding(
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          child: _inputViewOTPTextLineView(),
        ),
        _inputView('images/icon-password.png', 'Mật khẩu', false),
        _setupLineView(),
        _inputView('images/icon-password.png', 'Nhập lại mật khẩu', true),
        _setupLineView(),
        FlatButton(
          child: Text(
            'Gửi lại mã',
            style: TextStyle(fontSize: 16.0, color: orangeAppColor),
          ),
          onPressed: () {},
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 30.0),
          child: Container(),
        )
      ],
    );
  }

  Widget _setupLineView() {
    return Padding(
      padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 15.0),
      child: Divider(
        height: 1.0,
        color: orangeAppColor,
      ),
    );
  }

  Widget _inputView(String image, String placeholder, bool repass) {
    return Padding(
      padding: EdgeInsets.only(bottom: 15.0, left: 10.0, right: 10.0),
      child: Container(
        height: (MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top -
                MediaQuery.of(context).padding.bottom) /
            30,
        child: Row(
          children: <Widget>[
            Container(
              height: 30.0,
              width: 30.0,
              child: Image.asset(
                image,
                fit: BoxFit.contain,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: 12.0, right: 12.0, top: 2.0, bottom: 2.0),
              child: Container(color: orangeAppColor, width: 1.0),
            ),
            image == 'images/icon-vietnam-flag.png'
                ? Padding(
                    padding: EdgeInsets.only(right: 5.0),
                    child: Text('(+84)'),
                  )
                : Container(),
            Expanded(
              child: TextField(
                  focusNode: image != 'images/icon-password.png'
                      ? _focusPhoneNode
                      : repass ? _focusRepasswordNode : _focusPasswordNode,
                  keyboardType: image != 'images/icon-password.png'
                      ? TextInputType.phone
                      : TextInputType.text,
                  decoration: InputDecoration.collapsed(hintText: placeholder),
                  obscureText:
                      image == 'images/icon-password.png' ? true : false,
                  inputFormatters: image == 'images/icon-vietnam-flag.png'
                      ? [
                          LengthLimitingTextInputFormatter(11),
                        ]
                      : []),
            ),
            image == 'images/icon-vietnam-flag.png'
                ? Container(
                    height: 30.0,
                    width: 30.0,
                    child: Image.asset(
                      'images/icon-check.png',
                      fit: BoxFit.contain,
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  Widget _inputViewOTPText() {
    return Container(
      height: (MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.top -
              MediaQuery.of(context).padding.bottom) /
          30,
      width: MediaQuery.of(context).size.width * 9 / 10,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: TextField(
                  focusNode: _focusOTP1Node,
                  keyboardType: TextInputType.phone,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration.collapsed(hintText: ""),
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(1),
                  ]),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: TextField(
                  focusNode: _focusOTP2Node,
                  keyboardType: TextInputType.phone,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration.collapsed(hintText: ""),
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(1),
                  ]),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 5.0),
              child: TextField(
                  focusNode: _focusOTP3Node,
                  keyboardType: TextInputType.phone,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration.collapsed(hintText: ""),
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(1),
                  ]),
            ),
          ),
          Expanded(
            child: TextField(
                focusNode: _focusOTP4Node,
                keyboardType: TextInputType.phone,
                textAlign: TextAlign.center,
                decoration: InputDecoration.collapsed(hintText: ""),
                inputFormatters: [
                  LengthLimitingTextInputFormatter(1),
                ]),
          ),
        ],
      ),
    );
  }

  Widget _inputViewOTPTextLineView() {
    return Container(
      height: (MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.top -
              MediaQuery.of(context).padding.bottom) /
          30,
      width: MediaQuery.of(context).size.width * 9 / 10,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Divider(
              height: 1.0,
              color: orangeAppColor,
            ),
          ),
          Container(
            width: 5.0,
          ),
          Expanded(
            child: Divider(
              height: 1.0,
              color: orangeAppColor,
            ),
          ),
          Container(
            width: 5.0,
          ),
          Expanded(
            child: Divider(
              height: 1.0,
              color: orangeAppColor,
            ),
          ),
          Container(
            width: 5.0,
          ),
          Expanded(
            child: Divider(
              height: 1.0,
              color: orangeAppColor,
            ),
          ),
        ],
      ),
    );
  }

  Widget _setupTopBack() {
    return Padding(
      padding: EdgeInsets.only(top: 10.0),
      child: Container(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  height: 40.0,
                  width: MediaQuery.of(context).size.width / 8,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20.0),
                          bottomRight: Radius.circular(20.0)),
                      image: DecorationImage(
                          image: AssetImage('images/icon-back.png'),
                          fit: BoxFit.cover)),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 40.0,
                child: Center(
                  child: Text(
                    'Cập nhật mật khẩu',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _setupLoginView() {
    return FormKeyboardActions(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      actions: [
        KeyboardAction(
          focusNode: _focusPhoneNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusOTP1Node,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusOTP2Node,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusOTP3Node,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusOTP4Node,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusPasswordNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        KeyboardAction(
          focusNode: _focusRepasswordNode,
          closeWidget: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text(
              "Done",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
      child: SafeArea(
        child: Container(
            color: Colors.transparent,
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      _setupTopBack(),
                      Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: Center(
                            child: Text(
                              'Chúng tôi đã gửi mã OTP vào tài khoản của bạn.\nVui lòng nhập mã để xác định.',
                              textAlign: TextAlign.center,
                            ),
                          )),
                      Expanded(
                        flex: 2,
                        child: Container(
                            child: Center(
                                child: Wrap(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: _setupInputView(),
                            ),
                          ],
                        ))),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 20.0),
                          child: Container(
                            child: Text(
                              'Bằng cách Tiếp tục, bạn đồng ý với Điều khoản và \nChính sách quyền riêng tư của chúng tôi.',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[_setupBackground(), _setupLoginView()],
      ),
    ));
  }
}
