import 'package:flutter/material.dart';
import '../../../../ultility/hexColor.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:auto_size_text/auto_size_text.dart';
import '../../../login/login_page/src/ui/login_page.dart';
import '../../../../common/constant.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    FlutterStatusbarManager.setHidden(true);
    super.initState();
  }

  Widget _setupBackground() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: <Widget>[
          Container(
            color: HexColor("015B99"),
            height: 40.0,
          ),
          Container(
            height: MediaQuery.of(context).size.height - 40.0,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/startBackground.png'),
                    fit: BoxFit.fill)),
          ),
        ],
      ),
    );
  }

  Widget _setupStartButton() {
    return Positioned(
        bottom: 0.0,
        height: MediaQuery.of(context).size.height / 4,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: Container(
              width: MediaQuery.of(context).size.width / 3,
              height: MediaQuery.of(context).size.width / 9,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  border: Border.all(color: orangeAppColor, width: 2.0),
                  color: Colors.white,
                ),
                child: MaterialButton(
                  child: Padding(
                    padding: EdgeInsets.only(left: 5.0, right: 5.0),
                    child: AutoSizeText('BẮT ĐẦU',
                        style: TextStyle(color: orangeAppColor, fontSize: 25.0)),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        maintainState: false,
                        settings: RouteSettings(
                          isInitialRoute: true,
                        ),
                        builder: (context) => LoginPage()));
                  },
                ),
              )),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[_setupBackground(), _setupStartButton()],
        ),
      ),
    );
  }
}
